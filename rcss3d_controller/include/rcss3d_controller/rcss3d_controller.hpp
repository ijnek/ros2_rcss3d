// Copyright 2019 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef RCSS3D_CONTROLLER__RCSS3D_CONTROLLER_HPP_
#define RCSS3D_CONTROLLER__RCSS3D_CONTROLLER_HPP_

#include <sensor_msgs/msg/imu.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <rosgraph_msgs/msg/clock.hpp>
#include <rcss3d_controller_msgs/msg/joint_command.hpp>

#include <rclcpp/rclcpp.hpp>

#include <memory>
#include <vector>
#include <string>

#define SEXPRESSO_OPT_OUT_PIKESTYLE
#include "sexpresso/sexpresso.hpp"

#include "rcss3d_controller/visibility_control.h"

#include "rcss3d_controller/socket.hpp"
#include "rcss3d_controller/socketaddress.hpp"

namespace rcss3d_controller
{

class Rcss3DController : public rclcpp::Node
{
public:
  Rcss3DController();

  virtual ~Rcss3DController();

private:
  using Clock = rosgraph_msgs::msg::Clock;
  using Imu = sensor_msgs::msg::Imu;
  using JointState = sensor_msgs::msg::JointState;
  using JointCommand = rcss3d_controller_msgs::msg::JointCommand;

  static constexpr double deg2rad(double rad) {return rad * 3.141592654 / 180.0;}

  enum class State
  {
    CREATING,
    INITIALIZING,
    RUNNING
  };

  std::unique_ptr<Socket> socket_;
  std::unique_ptr<SocketAddress> socket_address_;

  std::vector<char> buffer_;

  uint32_t unum_;
  std::string team_;

  State state_;

  std::string imu_frame_;

  std::thread receive_thread_;
  std::atomic<bool> canceled_;

  rclcpp::Publisher<Clock>::SharedPtr clock_pub_;
  rclcpp::Publisher<Imu>::SharedPtr imu_pub_;
  rclcpp::Publisher<JointState>::SharedPtr joint_state_pub_;

  rclcpp::Subscription<JointCommand>::SharedPtr joint_command_sub_;

  void initSocket(std::string const & host, int port);
  void connect();
  void initConnection();
  uint32_t receive();
  void send(sexpresso::Sexp sexp, bool wrap = true);

  void create();
  void init();
  void handle();
};

}  // namespace rcss3d_controller

#endif  // RCSS3D_CONTROLLER__RCSS3D_CONTROLLER_HPP_
